package com.radar.shopping;

import javax.activation.DataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class HibernateSetting {
    @Bean
    public static org.hibernate.SessionFactory getSessionFactory() {

        if (sessionFactory == null) {
            try {
                org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration();
                // DataSourceBuilder datasource=DataSourceBuilder.create();

                java.util.Properties settings = new java.util.Properties();
                settings.put(org.hibernate.cfg.Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
                settings.put(org.hibernate.cfg.Environment.URL, "jdbc:mysql://127.0.0.1:3306/radar");
                settings.put(org.hibernate.cfg.Environment.USER, "user");
                settings.put(org.hibernate.cfg.Environment.PASS, "123@Qwe123");
                settings.put(org.hibernate.cfg.Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
                settings.put(org.hibernate.cfg.Environment.SHOW_SQL, "true");
                settings.put(org.hibernate.cfg.Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                settings.put(org.hibernate.cfg.Environment.POOL_SIZE, "10");
                configuration.setProperties(settings);

                addEntites(configuration);

                org.hibernate.service.ServiceRegistry serviceRegistry = new org.hibernate.boot.registry.StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (org.hibernate.HibernateException e) {
            }
        }
        return sessionFactory;
    }

    private static void addEntites(org.hibernate.cfg.Configuration configuration) {
        configuration.addAnnotatedClass(com.radar.shopping.entities.ShoppingEntity.class);
//        configuration.addAnnotatedClass(entities.BankAccountBean.class);
//        configuration.addAnnotatedClass(entities.EmployeeEntity.class);
//        configuration.addAnnotatedClass(entities.AccountEntity.class);
//        configuration.addPackage("entities");
    }

    private static org.hibernate.SessionFactory sessionFactory = null;
}