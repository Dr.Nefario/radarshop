/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.radar.shopping.controller;

import com.radar.shopping.daos.ShoppingDAO;
import java.util.List;

import com.radar.shopping.entities.ShoppingEntity;
import lombok.val;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class HomeController {

    @RequestMapping(value="/index")
    public List index() {
        ShoppingDAO shoppingDao=new ShoppingDAO();
        return shoppingDao.getShopping();
        //return "Hello World";
    }
    @RequestMapping(value = "/add", consumes = APPLICATION_JSON_VALUE)
    public String addShopping(@RequestBody ShoppingEntity product){
        ShoppingDAO shoppingDAO = new ShoppingDAO();
        try{
            return shoppingDAO.addProduct(product)+"";

        }catch (Exception ex){
            ex.printStackTrace();
            return 0+"";
        }

    }


}