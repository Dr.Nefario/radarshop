
package com.radar.shopping.daos;
import com.radar.shopping.HibernateSetting;
import com.radar.shopping.entities.ShoppingEntity;
import org.hibernate.Session;

public class ShoppingDAO {

    public ShoppingDAO() {
    }

    public java.util.List<ShoppingEntity> getShopping() {
        try (Session session = com.radar.shopping.HibernateSetting.getSessionFactory().openSession()) {
            return session.createQuery("FROM ShoppingEntity", ShoppingEntity.class).list();
        }catch(org.hibernate.HibernateException e){
            System.out.println(e);
            return null;
        }
    }

    public void listShopping() {
        try (org.hibernate.Session session =HibernateSetting.getSessionFactory().openSession()) {
//            tx = session.beginTransaction();
            java.util.List<ShoppingEntity> shopping = session.createQuery("from ShoppingEntity", ShoppingEntity.class).list();
            shopping.forEach(p -> System.out.println("Name: " + p.getName() + " --- " + "Type is: " + p.getType()));
        } catch (org.hibernate.HibernateException e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    public Integer addproduct(String name, short type) {
        Integer productID = null;
        try (org.hibernate.Session session = HibernateSetting.getSessionFactory().openSession()) {
            tx = session.beginTransaction();
            ShoppingEntity product = new ShoppingEntity(name, type);
            productID = (Integer) session.save(product);
            tx.commit();
            session.close();
        } catch (org.hibernate.HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        }
        return productID;
    }

    public Integer addProduct(ShoppingEntity product) {
        Integer productID = null;
        try (org.hibernate.Session session = HibernateSetting.getSessionFactory().openSession()) {
            tx = session.beginTransaction();
            productID = (Integer) session.save(product);
            tx.commit();
            session.close();
        } catch (org.hibernate.HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        }
        return productID;
    }

    public boolean updateProduct(Integer productID, String new_name, short new_type) {
        boolean result = false;
        try (org.hibernate.Session session = HibernateSetting.getSessionFactory().openSession()) {
            tx = session.beginTransaction();
            ShoppingEntity product = (ShoppingEntity) session.get(ShoppingEntity.class, productID);
            if (product != null) {
                product.setName(new_name);
                product.setType(new_type);
                session.update(product);
            } else {
                result = false;
            }
            tx.commit();
            session.close();
        } catch (org.hibernate.HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
            System.exit(0);
        }
        return result;
    }

    public boolean updateProduct(ShoppingEntity new_product) {
        ShoppingEntity product = null;
        boolean result = false;
        try (org.hibernate.Session session = HibernateSetting.getSessionFactory().openSession()) {
            tx = session.beginTransaction();
            product = (ShoppingEntity) session.get(ShoppingEntity.class, new_product.getId());
            if (product != null) {
                session.update(new_product);
            } else {
                result = false;
            }
            tx.commit();
            session.close();
        } catch (org.hibernate.HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
            System.exit(0);
        }
        return result;
    }

    public boolean deleteProduct(Integer productID) {
        ShoppingEntity product = null;
        boolean result = false;
        try (org.hibernate.Session session = HibernateSetting.getSessionFactory().openSession()) {
            tx = session.beginTransaction();
            product = (ShoppingEntity) session.get(ShoppingEntity.class, productID);
            if (product != null) {
                session.delete(product);
                result = true;
            } else {
                result = false;
            }
            tx.commit();
            session.close();
        } catch (org.hibernate.HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        }
        return result;
    }

    public boolean deleteProduct(ShoppingEntity product) {
        ShoppingEntity poductTemp = null;
        boolean result = false;
        try (org.hibernate.Session session = HibernateSetting.getSessionFactory().openSession()) {
            tx = session.beginTransaction();
            poductTemp = (ShoppingEntity) session.get(ShoppingEntity.class, product.getId());
            if (poductTemp != null) {
                session.delete(poductTemp);
                result = true;
            } else {
                result = false;
            }
            tx.commit();
            session.close();
        } catch (org.hibernate.HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        }
        return result;
    }

    public ShoppingEntity searchProduct(String name) {
        ShoppingEntity product = null;
        try (org.hibernate.Session session = HibernateSetting.getSessionFactory().openSession()) {
            tx = session.beginTransaction();
            org.hibernate.query.Query query = session.createQuery("from ShoppingEntity P WHERE P.name LIKE :pName");
            query.setParameter("pName", "%" + name + "%");
            java.util.List<ShoppingEntity> results = query.getResultList();
            if (results != null && !results.isEmpty()) {
                product = (ShoppingEntity) results.get(0);
            } else {
                return null;
            }
        } catch (org.hibernate.HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
            System.exit(0);
        }
        return product;
    }

    private org.hibernate.Transaction tx = null;
}