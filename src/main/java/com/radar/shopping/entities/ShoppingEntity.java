
package com.radar.shopping.entities;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity()
@Table(name="shopping")
@JsonPropertyOrder({"id","name","barcode"})
public class ShoppingEntity implements java.io.Serializable{


    public ShoppingEntity(){
        this.name="برنج";
        this.barcode =1258633345;
        this.type=0;
        this.parentId=0;
        this.Status=1;
    }
    public ShoppingEntity(String name,int ParentId){
        this.name="";
        this.parentId=0;
    }
    public ShoppingEntity(int Id,String name,int parentid){
        this.Id=0;
        this.name="";
        this.parentId=0;
    }

    public int getId(){
        return this.Id;
    }
    public void setId(int Id){}

    public String getName() {
        return name;
    }
    public void setName(String name){}


    public short getType() {
        return type;
    }
    public void setType(Short type){}

    // @Override
    public String tostring(){
        return null;
    }

    @javax.persistence.Id
    @javax.persistence.Column(name="id")
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Integer Id;

    @javax.persistence.Column(name="name")
    private String name;

    @javax.persistence.Column(name="barcode")
    private double barcode;

    @javax.persistence.Column(name="type")
    private short type;

    @javax.persistence.Column(name="parentid")
    private int parentId;

    @javax.persistence.Column(name="status")
    private short Status;


    public double getBarcode() {
        return barcode;
    }

    public void setBarcode(double Barcode) {
        this.barcode = Barcode;
    }

    public void setType(short Type) {
        this.type = Type;
    }

    public Integer getParentid() {
        return parentId;
    }

    public void setParentid(Integer Parentid) {
        this.parentId = Parentid;
    }

    public short getStatus() {
        return Status;
    }

    public void setStatus(short Status) {
        this.Status = Status;
    }


}